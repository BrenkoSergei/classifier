package com.irvil.textclassifier.classifier;

import static org.encog.persist.EncogDirectoryPersistence.loadObject;
import static org.encog.persist.EncogDirectoryPersistence.saveObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.irvil.textclassifier.model.Characteristic;
import com.irvil.textclassifier.model.CharacteristicValue;
import com.irvil.textclassifier.model.ClassifiableText;
import com.irvil.textclassifier.model.VocabularyWord;
import com.irvil.textclassifier.ngram.NGramStrategy;
import com.irvil.textclassifier.observer.Observable;
import com.irvil.textclassifier.observer.Observer;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.math3.util.Pair;
import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.Propagation;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.PersistError;

public class Classifier implements Observable {
    
    private static final double TRAINING_THRESHOLD = 0.3;

    private final Characteristic characteristic;
    private int inputLayerSize;
    private BasicNetwork network;
    private final int outputLayerSize;
    private final List<VocabularyWord> vocabulary;
    private final Map<Integer, CharacteristicValue> idToCategory;
    private final NGramStrategy nGramStrategy;
    private final List<Observer> observers = new ArrayList<>();
    private final Map<String, Integer> knownWordToIdInVocablurary = new HashMap<>();
    private final Map<String, Set<WordCharacteristic>> unknownWords = new HashMap<>();

    public Classifier(File trainedNetwork, Characteristic characteristic, List<VocabularyWord> vocabulary, NGramStrategy nGramStrategy) {
        if (characteristic == null ||
                characteristic.getName().equals("") ||
                characteristic.getPossibleValues() == null ||
                characteristic.getPossibleValues().size() == 0 ||
                vocabulary == null ||
                vocabulary.size() == 0 ||
                nGramStrategy == null) {
            throw new IllegalArgumentException();
        }

        this.characteristic = characteristic;
        this.vocabulary = vocabulary;

        idToCategory = characteristic.getPossibleValues().stream()
                .collect(Collectors.toMap(CharacteristicValue::getOrderNumber, Function.identity()));
        this.inputLayerSize = vocabulary.size();
        this.outputLayerSize = characteristic.getPossibleValues().size();
        this.nGramStrategy = nGramStrategy;

        if (trainedNetwork == null) {
            this.network = createNeuralNetwork(inputLayerSize, outputLayerSize);
            this.network.getStructure().finalizeStructure();
            this.network.reset();
        } else {
            // load neural network from file
            try {
                this.network = (BasicNetwork) loadObject(trainedNetwork);
            } catch (PersistError e) {
                throw new IllegalArgumentException();
            }
        }
    }

    public Classifier(Characteristic characteristic, List<VocabularyWord> vocabulary, NGramStrategy nGramStrategy) {
        this(null, characteristic, vocabulary, nGramStrategy);
    }

    public static void shutdown() {
        Encog.getInstance().shutdown();
    }

    private BasicNetwork createNeuralNetwork(int inputLayerSize, int outputLayerSize) {
        BasicNetwork network = new BasicNetwork();

        // input layer
        network.addLayer(new BasicLayer(null, true, inputLayerSize));

        // hidden layer
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputLayerSize / 4));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, inputLayerSize / 4 / 2));

        // output layer
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, outputLayerSize));

        return network;
    }

    public CharacteristicValue classify(ClassifiableText classifiableText) {
        double[] output = new double[outputLayerSize];

        // calculate output vector
        network.compute(getTextAsVectorOfWords(classifiableText), output);
        Encog.getInstance().shutdown();

        return convertVectorToCharacteristic(output);
    }

    public double[] classify(Set<WordCharacteristic> wordCharacteristics) {
        double[] output = new double[outputLayerSize];

        Set<WordCharacteristic> uniqueValues = nGramStrategy.getNGram(wordCharacteristics);
        // calculate output vector
        network.compute(getTextAsVectorOfWords(uniqueValues), output);
        Encog.getInstance().shutdown();

        return output;
    }

    private double[] getTextAsVectorOfWords(Set<WordCharacteristic> words) {
        double[] vector = new double[inputLayerSize];

        Set<WordCharacteristic> knownWords = new HashSet<>();
        Set<String> unknownWords = new HashSet<>();
        for (WordCharacteristic wordCharacteristic : words) {
            VocabularyWord vocabularyWord = findWordInVocabulary(wordCharacteristic.getWord());

            if (vocabularyWord != null) {
                vector[vocabularyWord.getId() - 1] = 1;
                knownWords.add(wordCharacteristic);
            } else {
                unknownWords.add(wordCharacteristic.getWord());
            }
        }

        unknownWords.forEach(unknownWord -> {
            this.unknownWords.putIfAbsent(unknownWord, new HashSet<>());
            this.unknownWords.get(unknownWord).addAll(knownWords);
        });

        return vector;
    }

    public void retrain() {
        if (!unknownWords.isEmpty()) {
            Map<String, CharacteristicValue> wordToCategory = getKnownWordsCategories();

            List<String> learnedWords = learnWordsIfPossible(wordToCategory);
            learnedWords.forEach(unknownWords::remove);

            List<ClassifiableText> trainingData = wordToCategory.entrySet().stream()
                    .map(stringCharacteristicValueEntry -> {
                        String word = stringCharacteristicValueEntry.getKey();
                        Characteristic characteristic = new Characteristic("Method", Collections.singleton(stringCharacteristicValueEntry.getValue()));
                        Map<Characteristic, CharacteristicValue> characteristicMap = Collections.singletonMap(characteristic, stringCharacteristicValueEntry.getValue());

                        return new ClassifiableText(word, characteristicMap);
                    })
                    .collect(Collectors.toList());

            BasicNetwork newNeuralNetwork = createNeuralNetwork(vocabulary.size(), outputLayerSize);
            newNeuralNetwork.getStructure().finalizeStructure();
            newNeuralNetwork.reset();

            for (int i = 0; i < this.network.getLayerCount() - 1; ++i) {
                for (int j = 0; j < this.network.getLayerNeuronCount(i); ++j) {
                    for (int k = 0; k < this.network.getLayerNeuronCount(i + 1); ++k) {
                        newNeuralNetwork.setWeight(i, j, k, this.network.getWeight(i, j, k));
//                        System.out.println(String.format("(%4d %4d %4d) %1.6f", i, j, k, this.network.getWeight(i, j, k)));
                    }
                }
            }
            printWeights();

            this.network = newNeuralNetwork;
            inputLayerSize = vocabulary.size();
            train(trainingData);
        }
    }
    
    public void printWeights() {
        for (int i = 0; i < this.network.getLayerCount() - 1; ++i) {
            for (int j = 0; j < this.network.getLayerNeuronCount(i); ++j) {
                for (int k = 0; k < this.network.getLayerNeuronCount(i + 1); ++k) {
                    System.out.println(String.format("(%4d %4d %4d) %1.6f", i, j, k, this.network.getWeight(i, j, k)));
                }
            }
        }
    }

    private Map<String, CharacteristicValue> getKnownWordsCategories() {
        double[] knownWords = new double[inputLayerSize];
        double[] output = new double[outputLayerSize];
        Map<String, CharacteristicValue> wordToCategory = new HashMap<>(inputLayerSize);
        this.knownWordToIdInVocablurary.forEach((vocabularyWord, id) -> {
            knownWords[id - 1] = 1;
            network.compute(knownWords, output);
            knownWords[id - 1] = 0;

            int maxId = getIdOfMaxValue(output);
            wordToCategory.put(vocabularyWord, idToCategory.get(maxId));
        });
        return wordToCategory;
    }

    private List<String> learnWordsIfPossible(Map<String, CharacteristicValue> wordToCategory) {
        List<String> learnedWords = new ArrayList<>(unknownWords.size());
        unknownWords.forEach((unknownWord, wordCharacteristics) -> {
            if (!CollectionUtils.isEmpty(wordCharacteristics)) {
                CharacteristicValue category = wordCharacteristics.stream()
                        .collect(Collectors.toMap(
                                wordCharacteristic -> wordToCategory.get(wordCharacteristic.getWord()),
                                wordCharacteristic -> new Pair<>(wordCharacteristic.getOccurrenceFrequency(), 1),
                                (value1, value2) -> new Pair<>(value1.getKey() + value2.getKey(), value1.getValue() + value2.getValue()))
                        )
                        .entrySet().stream()
                        .sorted(Comparator.comparing(categoryToPair -> {
                            Pair<Double, Integer> pair = categoryToPair.getValue();
                            return pair.getKey() / pair.getValue();
                        }))
                        .filter(characteristicValueDoubleEntry -> {
                            Pair<Double, Integer> pair = characteristicValueDoubleEntry.getValue();
                            return pair.getKey() / pair.getValue() >= TRAINING_THRESHOLD;
                        })
                        .map(Map.Entry::getKey)
                        .findFirst().orElse(null);
                if (category != null) {
                    vocabulary.add(new VocabularyWord(vocabulary.size(), unknownWord));
                    wordToCategory.put(unknownWord, category);
                    learnedWords.add(unknownWord);
                }
            }
        });
        
        return learnedWords;
    }

    private double[] getTextAsVectorOfWords(ClassifiableText classifiableText) {
        double[] vector = new double[inputLayerSize];

        Set<String> uniqueValues = nGramStrategy.getNGram(classifiableText.getText());
        for (String word : uniqueValues) {
            VocabularyWord vw = findWordInVocabulary(word);

            if (vw != null) {
                vector[vw.getId() - 1] = 1;
            }
        }

        return vector;
    }

    private CharacteristicValue convertVectorToCharacteristic(double[] vector) {
        int idOfMaxValue = getIdOfMaxValue(vector);

        for (CharacteristicValue characteristicValue : characteristic.getPossibleValues()) {
            if (characteristicValue.getOrderNumber() == idOfMaxValue) {
                return characteristicValue;
            }
        }

        return null;
    }

    private int getIdOfMaxValue(double[] vector) {
        int indexOfMaxValue = 0;
        double maxValue = vector[0];

        for (int i = 1; i < vector.length; i++) {
            if (vector[i] > maxValue) {
                maxValue = vector[i];
                indexOfMaxValue = i;
            }
        }

        return indexOfMaxValue + 1;
    }

    public void saveTrainedClassifier(File trainedNetwork) {
        saveObject(trainedNetwork, network);
        notifyObservers("Trained Classifier for '" + characteristic.getName() + "' characteristic saved. Wait...");
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public void train(List<ClassifiableText> classifiableTexts) {
        double[][] input = getInput(classifiableTexts);
        double[][] ideal = getIdeal(classifiableTexts);

        Propagation train = new ResilientPropagation(network, new BasicMLDataSet(input, ideal));
        train.setThreadCount(16);

        do {
            train.iteration();
            notifyObservers("Training Classifier for '" + characteristic.getName() + "' characteristic. Errors: " + String.format("%.2f", train.getError() * 100) + "%. Wait...");
        } while (train.getError() > 0.00001);

        train.finishTraining();
        notifyObservers("Classifier for '" + characteristic.getName() + "' characteristic trained. Wait...");
    }

    private double[][] getInput(List<ClassifiableText> classifiableTexts) {
        double[][] input = new double[classifiableTexts.size()][inputLayerSize];

        int i = 0;
        knownWordToIdInVocablurary.clear();
        for (ClassifiableText classifiableText : classifiableTexts) {
            double[] vector = new double[inputLayerSize];

            Set<String> uniqueValues = nGramStrategy.getNGram(classifiableText.getText());
            for (String word : uniqueValues) {
                VocabularyWord vw = findWordInVocabulary(word);

                if (vw != null) {
                    knownWordToIdInVocablurary.put(vw.getValue(), vw.getId());
                    vector[vw.getId() - 1] = 1;
                }
            }

            input[i++] = vector;
        }

        return input;
    }

    private double[][] getIdeal(List<ClassifiableText> classifiableTexts) {
        double[][] ideal = new double[classifiableTexts.size()][outputLayerSize];

        int i = 0;
        for (ClassifiableText classifiableText : classifiableTexts) {
            ideal[i++] = getCharacteristicAsVector(classifiableText);
        }

        return ideal;
    }
    // example:

    // count = 5; id = 4;
    // vector = {0, 0, 0, 1, 0}
    private double[] getCharacteristicAsVector(ClassifiableText classifiableText) {
        double[] vector = new double[outputLayerSize];
        vector[classifiableText.getCharacteristicValue(characteristic.getName()).getOrderNumber() - 1] = 1;
        return vector;
    }

    private VocabularyWord findWordInVocabulary(String word) {
        try {
            return vocabulary.get(vocabulary.indexOf(new VocabularyWord(word)));
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return characteristic.getName() + "NeuralNetworkClassifier";
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers(String text) {
        for (Observer o : observers) {
            o.update(text);
        }
    }
}