package com.irvil.textclassifier.classifier;

public class WordCharacteristic {
    
    private String word;
    private double occurrenceFrequency;

    public WordCharacteristic(String word, double occurrenceFrequency) {
        this.word = word;
        this.occurrenceFrequency = occurrenceFrequency;
    }

    public String getWord() {
        return word;
    }

    public double getOccurrenceFrequency() {
        return occurrenceFrequency;
    }
}
